package ejercicio1;

public class ejercicio1 {

	public static void main(String[] args) {
		// 1 - Declara dos variables num�ricas (con el valor que desees), 
		// muestra por consola la suma, resta, multiplicaci�n, divisi�n y m�dulo (resto de la divisi�n).
		
		// Declaraci�n de las variables
		
		int a = 20;
		
		int b = 5;
		
		// Resultado de las operaciones (directamente con System.out.print para reducir el tama�o)
		
		System.out.println("Suma: " + (a + b));
		
		System.out.println("Resta: " + (a - b));
		
		System.out.println("Multiplicaci�n: " + (a * b));
		
		System.out.println("Divisi�n: " + (a / b));
		
		System.out.println("M�dulo (resto de la divisi�n): " + (a % b));

	}

}
